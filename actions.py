# -*- coding: utf-8 -*-
from django.contrib import admin, messages
from django.contrib.admin.models import LogEntry
from django.contrib.admin.models import CHANGE
from django.utils.translation import ugettext_lazy as _
from django.views.generic.edit import FormView
from django.utils.functional import cached_property
from django.utils.safestring import mark_safe
from django import forms
from django.utils.encoding import force_unicode


class ActionView(FormView):
    template_name = 'admin/confirmation.html'
    callback_template = ''
    allow_empty = False
    short_description = ''
    description = ''
    counter = 0
    chancel_message = _(u'Action cancelled')
    finished_message = ''

    @cached_property
    def answer(self):
        return YesNoForm(**super(ActionView, self).get_form_kwargs())

    def post(self, request, *args, **kwargs):
        if self.answer.is_valid():
            if self.answer.is_positiv:
                if self.get_form_class():  # answer is Positiv, processing with or without form
                    return super(ActionView, self).post(request, *args, **kwargs)  # form processing
                return self.processing(*args, **kwargs)  # processing without form
            return self.finished(chancel=True)  # answer is Negativ
        return self.get(request, *args, **kwargs)  # answer is not valid

    def get(self, request, *args, **kwargs):
        if self.get_form_class():
            self.request.method = 'GET'
            return super(ActionView, self).get(request, *args, **kwargs)
        return self.render_to_response(self.get_context_data(*args, **kwargs))

    def form_valid(self, *args, **kwargs):
        """ make ready form.cleaned_data for processing """
        return self.processing(*args, **kwargs)

    def processing(self, *args, **kwargs):
        """ make main process """
        return self.finished(*args, **kwargs)

    def finished(self, *args, **kwargs):
        """ send message after process and return None """
        message = kwargs.get('chancel') and self.chancel_message or kwargs.get('message') or self.finished_message or ''
        if message:
            self.info(message, counter=self.counter)

    def get_form(self, form_class, **kwargs):
        form = super(ActionView, self).get_form(form_class)
        if hasattr(form, 'prepeare'):
            return form.prepeare(request=self.request, **dict({'prefix': self.get_prefix()}, **kwargs))
        form.request = self.request
        return form

    def get_prefix(self):
        return getattr(self, 'prefix', None)

    @classonlymethod
    def as_view(cls, **initkwargs):
        response = super(ActionView, cls).as_view(**initkwargs)
        response.short_description = cls.short_description
        return response

    def dispatch(self, admin, request, queryset, *args, **kwargs):
        self.model = admin.model
        self.queryset = queryset
        return super(ActionView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self, **kwargs):
        return getattr(self, 'queryset', None) or []

    def get_object(self):
        for obj in self.get_queryset():
            return obj

    def log(self, obj, user=None, action_flag=CHANGE, change_message=''):
        # TODO change WURDE GEÄNDERT
        # TODO change ct
        LogEntry.objects.log_action(
            user_id=user.pk if user else self.request.user.pk, content_type_id=obj.ct().id,
            object_id=obj.pk, object_repr=unicode(obj),
            action_flag=action_flag, change_message=change_message or _(u'%s wurde geändert') % type(obj))

    def info(self, message, message_type='info', **kwargs):
        message = unicode(message.format(**kwargs))
        getattr(messages, message_type, messages.info)(self.request, message)
        return message

    def error(self, message, **kwargs):
        return self.info(message, message_type='error', **kwargs)

    def warning(self, message, **kwargs):
        return self.info(message, message_type='warning', **kwargs)

    def get_context_data(self, **kwargs):
        context = {
            'title': '%s. %s' % (unicode(self.short_description), unicode(_(u'Are you sure?'))),
            'action_short_description': self.short_description,
            'action_description': self.description,
            'action_name': type(self).__name__,
            'objects_name': force_unicode(self.model._meta.verbose_name_plural),
            'action_objects': [self.get_queryset()],
            'callback_template': self.callback_template,
            'queryset': self.get_queryset(),
            'opts': self.model._meta,
            'app_label': self.model._meta.app_label,
            'action_checkbox_name': admin.helpers.ACTION_CHECKBOX_NAME,
            'answer_form': self.answer,
        }
        context.update(kwargs)
        return context

class YesNoForm(forms.Form):

    method = 'POST'
    process_url = ''
    form_template = 'core/extraform.html'
    prefix = 'YesNoForm'
    button_template = '<button type="{submit}" name="answer" value="{value}">{title}</button>'.format
    answer_help_text = answer_label = ''
    POSITIV = 'yes'
    NEGATIV = 'no'
    POSITIV_TEXT = _('Yes, I\'m sure')
    NEGATIV_TEXT = _('No, I\'m not sure')
    CHOICES = [(POSITIV, POSITIV_TEXT), (NEGATIV, NEGATIV_TEXT)]

    base_fields = declared_fields = forms.forms.get_declared_fields([],
                                                                    {'answer': forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect())},
                                                                    with_base_fields=False)

    def prepeare(self, *args, **kwargs):
        for key, val in kwargs.items():
            setattr(self, key, val)
        self.args = args
        answer = self.fields['answer']
        answer.choices = [self._get_choice('POSITIV'), self._get_choice('NEGATIV')]
        answer.label = self.answer_label
        answer.help_text = self.answer_help_text
        return self

    @cached_property
    def is_positiv(self):
        return self.return_cleaned_field() == self.POSITIV

    def _get_choice(self, choice):
        return getattr(self, choice), getattr(self, '{}_TEXT'.format(choice))

    def _render_button(self, value, text):
        return mark_safe(self.button_template(submit='submit', value=value, title=unicode(text)))

    def render_button_positive(self):
        return self._render_button(*self._get_choice('POSITIV'))

    def render_button_negative(self):
        return self._render_button(*self._get_choice('NEGATIV'))

    def return_cleaned_data(self, *args, **kwargs):
        return self.cleaned_data if self.is_valid() else {}

    def return_cleaned_field(self, **kwargs):
        return self.return_cleaned_data().get(self.return_fild_name_for_answer(**kwargs))

    def return_fild_name_for_answer(self, **kwargs):
        return kwargs.get('field') or hasattr(self, 'field') and self.field or self.fields.keys()[0]